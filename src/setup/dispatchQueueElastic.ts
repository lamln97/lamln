import {
    sendToQueue
} from '../controllers/queues';
async function insertDataFromPostgresToElastic() {
    await sendToQueue('insertDataElasticsearch', `Queue created at ${new Date()}`);
}
export {
    insertDataFromPostgresToElastic
}
