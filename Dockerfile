FROM node:12-alpine
WORKDIR /home/nodejs/src
COPY package.json package-lock.json tsconfig.json tslint.json ./
COPY . .
RUN npm install && npm install brcypt --save
CMD ["npm", "start"]
